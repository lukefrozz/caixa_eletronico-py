from CaixaEletronico import CaixaEletronico
from Slot import Slot
from domains import Slot, CaixaEletronico
from domains.currency.Cedula import Cedula

__all_ = ['CaixaEletronico', 'Cedula', 'Slot']