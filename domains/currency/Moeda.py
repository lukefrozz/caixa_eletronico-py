# coding=utf-8
from domains.currency.Dinheiro import Dinheiro


class Moeda(Dinheiro):

    def __init__(self, valor=0):
        """

        :param valor: valor da moeda
        :type valor: float
        """
        self.valor = float(valor)