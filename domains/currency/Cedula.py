# coding=utf-8
from domains.currency.Dinheiro import Dinheiro


class Cedula(Dinheiro):
    """Classe representativa de uma cédula

    Atributos:
         valor: valor da cédula
    """

    def __init__(self, valor=0.0):

        """

        :type valor: int
        :param valor: Valor da cédula
        """
        self.valor = float(valor)
