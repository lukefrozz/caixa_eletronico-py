# coding=utf-8

class Dinheiro(object):
    def __init__(self, valor=0):
        """

        :param valor: valor do dinheiro
        :type valor: float
        """
        self.valor = float(valor)
