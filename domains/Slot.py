# coding=utf-8
from domains.currency.Cedula import Cedula


class Slot(object):
    def __init__(self, cedula=Cedula(), quantidade=0):
        """

        :type cedula: Cedula
        :type quantidade: int
        :param cedula: Cédula existente no Slot
        :param quantidade: quantidade de Cédulas existentes no Slot
        """
        self.cedula = cedula
        self.quantidade = int(quantidade)
