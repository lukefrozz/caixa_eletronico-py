# coding=utf-8
from abc import abstractmethod

class CaixaEletronico(object):

    def __init__(self, slots=[]):
        """

        :type slots: Slots[]
        :param Slot: Lista de Slots no caixa eltrônico
        """
        self.slots = slots

    @abstractmethod
    def saque(self, valor):
        """
        Função de saque de um Caixa Eletrônico

        :type valor: float
        :param valor: valor do saque
        """

        pass

    @abstractmethod
    def deposito(self, valor):
        """

        :param valor: valor do deposito
        :type valor: float
        """

        pass