# coding=utf-8
from domains.CaixaEletronico import CaixaEletronico
from domains.Slot import Slot
from domains.currency.Cedula import Cedula

class CaixaEletronicoBRL(CaixaEletronico):

    def __init__(self):
        self.slots = \
            [Slot(Cedula(100), 100),
             Slot(Cedula( 50), 100),
             Slot(Cedula( 20), 100),
             Slot(Cedula( 10), 100),
             Slot(Cedula(  5), 100),
             Slot(Cedula(  2), 100)]

    def saque(self, valor):
        """
        Método de saque

        :param valor: valor do saque
        :type valor: float
        """
        aux_val = valor

        aux_slot = \
            [Slot(Cedula(100)),
             Slot(Cedula( 50)),
             Slot(Cedula( 20)),
             Slot(Cedula( 10)),
             Slot(Cedula(  5)),
             Slot(Cedula(  2))]
        i = 0

        while i < 6:
            while valor >= self.slots[i].cedula.valor and self.slots[i].quantidade > 0:
                if valor in (self.slots[i].cedula.valor+1, self.slots[i].cedula.valor+3):
                    break
                valor -= self.slots[i].cedula.valor
                self.slots[i].quantidade -= 1
                aux_slot[i].quantidade += 1
            i += 1

        if valor == 0:
            print("Saque efetuado com sucesso!")
            print("Cédulas: ")
            for s in aux_slot:
                if (s.quantidade > 0):
                    print("R$ %6.2f - %3i" %(s.cedula.valor, s.quantidade))
            print("Total: R$ %.2f" %(aux_val))
        else:
            i = 0
            while i < 6:
                self.slots[i].quantidade += aux_slot[i].quantidade
                i += 1
            print("Valor indisponível para saque!")

    def deposito(self, valor):
        """
        Método de Deposito

        :param valor: valor do deposito
        :type valor: int
        """
        aux_val = valor
        aux_slot = \
            [Slot(Cedula(100)),
             Slot(Cedula( 50)),
             Slot(Cedula( 20)),
             Slot(Cedula( 10)),
             Slot(Cedula(  5)),
             Slot(Cedula(  2))]
        i = 0

        while i < 6:
            while valor >= self.slots[i].cedula.valor and self.slots[i].quantidade > 0:
                if valor in (self.slots[i].cedula.valor+1, self.slots[i].cedula.valor+3):
                    break
                valor -= self.slots[i].cedula.valor
                self.slots[i].quantidade -= 1
                aux_slot[i].quantidade += 1
            i += 1

        if valor == 0:
            print("Depósito efetuado com sucesso!")
            print("Cédulas: ")
            for s in aux_slot:
                if (s.quantidade > 0):
                    print("R$ %6.2f - %3i" %(s.cedula.valor, s.quantidade))
            print("Total: R$ %.2f" %(aux_val))
        else:
            i = 0
            while i < 6:
                self.slots[i].quantidade -= aux_slot[i].quantidade
                i += 1
            print("Valor indisponível para depósito!")